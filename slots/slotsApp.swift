//
//  slotsApp.swift
//  slots
//
//  Created by Henry Delgado on 11/14/21.
//

import SwiftUI

@main
struct slotsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
